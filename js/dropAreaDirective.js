/**
 * Created by Bruce on 5/4/2014.
 */
angular.module('myApp')
    .service('dropSvc', ['$rootScope', function ($rootScope) {
        var self = this;
        self.draggingData = null;
        this.setDragContext = function (dragData, dropEndEventName, dragDataName, dropCallback, dropTargetValue) {
            var struct = {
                dropEndEventName: dropEndEventName,
                dragDataName: dragDataName,
                dropCallback: dropCallback,
                dropTargetValue: dropTargetValue,
                dragData: dragData
            };
            self.draggingData = dragData;
            $rootScope.$broadcast('dragContextChanged', struct);
        };
        this.dragEnded = function() {
            self.draggingData = null;
          $rootScope.$broadcast('dragEnded');
        };
        this.getDragData = function() {
          return self.draggingData;
        };
    }]);


angular.module('myApp')
.directive('dropArea', [function() {
    return {
        restrict: 'E',
        scope: { },
        controller: ['$scope', function(scope) {
            scope.dropEndEventName = 'dropEnd';
            scope.dragDataName = 'dragData';
            scope.dropCallback = angular.noop;
            scope.dropTargetValue = null;
            scope.dragging = false;

            scope.$on('dragContextChanged', function(event, input) {
                console.log('context changed');
                if(input.dropEndEventName) {
                    scope.dropEndEventName = input.dropEndEventName;
                }
                if(input.dragDataName) {
                    scope.dragDataName = input.dragDataName;
                }
                if(input.dropCallback) {
                    scope.dropCallback = input.dropCallback;
                }
                if(input.dropTargetValue) {
                    scope.dropTargetValue = input.dropTargetValue;
                }
                scope.$apply(function(){
                    scope.dragging = true;
                });
            });

            scope.$on('dragEnded', function(event) {
                scope.dropEndEventName = 'dropEnd';
                scope.dragDataName = 'dragData';
                scope.dropCallback = angular.noop;
                scope.dropTargetValue = null;
                scope.$apply(function(){
                    scope.dragging = false;
                });
            });
        }],
        link: function (scope, element, attrs) {

            var dnD = {
                handleDropleave: function (e) {
                    if (e.preventDefault) e.preventDefault(); // allows us to drop
                    var dataTransfer = e.originalEvent.dataTransfer.types;
                    if (dataTransfer != null && dataTransfer.length > 0) {
                        for (var i = 0; i < dataTransfer.length; i++) {
                            if (dataTransfer[i] == scope.dragDataName) {
                                element.removeClass("dragOver");
                                break;
                            }
                        }
                    }
                },

                handleDragEnter: function (e) {
                    if (e.preventDefault) e.preventDefault(); // allows us to drop
                    var dataTransfer = e.originalEvent.dataTransfer.types;
                    if (dataTransfer != null && dataTransfer.length > 0) {
                        for (var i = 0; i < dataTransfer.length; i++) {
                            if (dataTransfer[i] == scope.dragDataName) {
                                element.addClass("dragOver");
                                break;
                            }
                        }
                    }
                },

                handleDragOver: function (e) {
                    if (e.preventDefault) e.preventDefault(); // allows us to drop
                    var dataTransfer = e.originalEvent.dataTransfer.types;
                    if (dataTransfer != null && dataTransfer.length > 0) {
                        for (var i = 0; i < dataTransfer.length; i++) {
                            if (dataTransfer[i] == scope.dragDataName) {
                                element.addClass("dragOver");
                                break;
                            }
                        }
                    }
                    return false;
                },

                handleDropped: function (e) {
                    if (e.stopPropagation) e.stopPropagation(); // stops the browser from redirecting..

                    var jsonDataStr = e.originalEvent.dataTransfer.getData(scope.dragDataName);
                    if (jsonDataStr) {
                        var jsonData = angular.fromJson(jsonDataStr);
                        scope.dropCallback(jsonData, scope.dropTargetValue);
                    }
                    element.removeClass("dragOver"); // for removing highlighting effect on droppable object
                    scope.$emit(scope.dropEndEventName); //used to show/hide drop areas during drag
                    console.log('dropped item: ' + scope.dropTargetValue);
                    return false;
                }
            };
            element.bind("dragenter", dnD.handleDragEnter);
            element.bind("dragover", dnD.handleDragOver);
            element.bind("dragleave", dnD.handleDropleave);
            element.bind("drop", dnD.handleDropped);
        },
        template:
            "<div data-drop=\"true\" ng-model='list4' jqyoui-droppable=\"{multiple:true}\"</div>"
//            "<div class=\"col-md-1 well text-center text-muted\" ng-style=\"{border: (dragging && 'dashed') || 'none'}\">" +
//                "Drop Here "+
//            "</div>"
    };
    }]);