/**
 * Created by Bruce on 5/5/2014.
 */
angular.module('myApp')
.directive("itemDrag", ['dropSvc', function (dropSvc) {
    return {
        restrict: 'A',
        scope: {
            dragStartEventName: '@',
            dragEndEventName: '@',
            dragDataName: '@',
            dropEndEventName: '@',
            dragData: '=',
            dropCallback: '='
        },
        link: function (scope, jElm, attrs) {
            var dnd = {
                handleDragStart: function (e) {
                    this.style.opacity = '1';
                    // e.originalEvent will return the native javascript event as opposed to jQuery wrapped event
                    e.originalEvent.dataTransfer.effectAllowed = 'copy';
                    //payload from the draggable object
                    e.originalEvent.dataTransfer.setData(scope.dragDataName, angular.toJson(scope.dragData)); // required otherwise doesn't work
                    scope.$emit(scope.dragStartEventName); //used to show/hide drop areas during drag
                    dropSvc.setDragContext(scope.dropEndEventName, scope.dragDataName, scope.dropCallback, scope.dragData);
                },
                handleDragEnd: function (e) {
                    this.style.opacity = "1";
                    e.preventDefault();
                    scope.$emit(scope.dragEndEventName); //used to show/hide drop areas during drag
                    dropSvc.dragEnded();
                }
            };
            jElm.bind("dragstart", dnd.handleDragStart);
            jElm.bind("dragend", dnd.handleDragEnd);
        }
    }
}]);