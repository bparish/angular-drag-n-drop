/**
 * Created by Bruce on 5/4/2014.
 */
angular.module('myApp')
.controller('myController',['$scope', 'dropSvc', function(scope, dropSvc){
        scope.items = ['a', 'b', 'c', 'd'];
        scope.droppedList = [];
        scope.dragging = false;

        scope.$on('dragContextChanged', function(event, input) {
            scope.dragging = true;
        });

        scope.$on('dragEnded', function(event) {
           scope.dragging = false;
        });

        scope.onDrop = function(event, ui){
            var data = dropSvc.getDragData();
            if (data != undefined)
            scope.droppedList.push({value: data});
//            if(scope.list4.length > 0) {
//                scope.droppedList.push({value: scope.list4[0]});
//                scope.list4.length = 0;
//            }
        };

        scope.dragStart = function(event, ui,data) {
            dropSvc.setDragContext(data);
        };

        scope.dragEnd = function() {
            dropSvc.dragEnded();
        };
    }]);